# uniapp-vue3-uview-plus3.0-template

#### 介绍
基于 `uniapp` + `vue3` + `pinia` + `uview-plus` 3.0 的框架搭建的移动端小程序、app 开发框架，脚本使用的是 js

#### 技术文档

1.  [uni-app官网](https://uniapp.dcloud.net.cn/api/)

2.  [Vue.js](https://cn.vuejs.org/)

3.  [uview-plus 3.0](https://uiadmin.net/uview-plus/)

#### 其他资源库
以下是一些开发中常会使用的前端开发资源库
1. [阿里字体图标库](https://www.iconfont.cn/)，可根据 uniapp 官方 [uni-icons 图标扩展](https://uniapp.dcloud.net.cn/component/uniui/uni-icons.html#%E8%8E%B7%E5%8F%96%E5%9B%BE%E6%A0%87)的方式一键导入所需图标

2. [iconify图标库](https://icon-sets.iconify.design/)，拥有更多的图标资源和 `SVG` 格式的动图资源，能提供更丰富的图标资源，提供了图标资源的多种使用方式，如 `CSS` 方式、`SVG` 方式、`PNG` 图片格式等

3. 小编开发中常用前端知识点的[汇总博客](https://blog.csdn.net/qq_43551801/article/details/120768322)，零散记录了前端一些常用的开发知识点

