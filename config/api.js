const http = uni.$u.http

// post请求
export const weixinLogin = (params, config = {}) => http.post('/ai-auth/wechat/login', params, config)

// get请求，注意：get请求的配置等，都在第二个参数中
export const getUserInfo = (data) => http.get('/system/user/getInfo', data)
