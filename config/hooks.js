import {
	weixinLogin,
} from './api.js'
import {
	store
} from '@/store/index.js'

/**
 * 吊起微信支付
 * @param {Object} data 支付参数对象至少包含：timeStamp，nonceStr，package，signType，paySign
 * @param {String} successTip 支付成功后的提示文本，默认：支付成功
 * @param {String} failTip 支付失败 后的提示文本，默认：支付未完成
 */
export function wxPay(data, successTip = '支付成功', failTip = '支付未完成') {
	return new Promise((resolve, reject) => {
		if (!data?.timeStamp || !data?.nonceStr || !data?.package || !data?.signType || !data?.paySign)
			return resolve(false)
		uni.requestPayment({
			provider: 'wxpay',
			timeStamp: data.timeStamp,
			nonceStr: data.nonceStr,
			package: data.package,
			signType: data.signType,
			paySign: data.paySign,
			success(result) {
				console.log('wxPay-支付完成', result)
				uni.showToast({
					icon: 'success',
					title: successTip
				})
				resolve(true)
			},
			fail(e) {
				console.error('wxPay-支付未完成', e)
				uni.$u.toast(failTip)
				resolve(false)
			}
		})
	})
}


// 封装的微信登录方法
export async function useWXLogin() {
	// 先尝试从本地存储中读取用户登录后保存的 token ，如果读取到了则表示已经登录过了
	// if (store.token) return true
	// 未登录则执行以下登录的逻辑
	let params = {}
	// 1.调用 uni.getUserProfile() 询问用户是否同意授权登录
	/*
	try {
		const res = await uni.getUserProfile({
			desc: '用户登录',
			lang: 'zh_CN'
		})
		// console.log('获取信息', res); // res[1].userInfo 即可获取到用户的基本信息：头像、名称等
		params.nickname = res.userInfo.nickName
		params.gender = res.userInfo.gender;
		params.avatar = res.userInfo.avatarUrl
	} catch (e) {
		// console.error(e);
		// 用户取消授权的场景
		uni.showToast({
			title: '已取消授权',
			duration: 2000,
			icon: 'none'
		})
		return false
	}
	*/
	uni.showLoading({
		mask: true,
		title: '登录中...'
	})
	// console.log(JSON.stringify(res.userInfo)); // res 中就包含了 code 字段值
	// 2.调用 uni.login() 获取用户 code
	const res2 = await uni.login({
		provider: 'weixin', // 登录服务提供商,这里是微信
	})
	// console.log('获取code', res2)
	params.code = res2.code
	console.log(params);
	// 3.调用登录 API 接口进行登录
	const res3 = await weixinLogin(params)
	if (res3.code !== 200) {
		uni.hideLoading()
		return false
	}
	console.log('登录接口回调', res3);
	// 保存返回的 token 到 store 中（同时也会存储到本地存储中）
	store.setToken(res3.data.access_token)
	let data = await getUserDataDetail()
	if (data.code == 200) {
		// 保存返回的 用户信息 到 store 中（同时也会存储到本地存储中）
		store.setUserInfo(data.data)
	}
	uni.hideLoading()
	return true
}

/**
 * u-upload 组件上传图片的方法
 * @param {Object} event 是选择的图片对象，对象的 url 属性为图片的本地路径，如果选择的是多张图片请使用便利逐一传入
 */
export async function uploadImg(event) {
	if (event.hasOwnProperty('file')) event = event.file
	if (typeof event != 'object' || (!event?.url && !event[0]?.url)) return console.error(
		'hooks.js function uploadImg 接收到的参数有误！', event);
	console.log('上传图片', event);
	uni.showLoading({
		title: '上传中'
	})
	let url = event.length == undefined ? event.url : event[0].url
	return uploadFilePromise(url)
}
// 将图片上传到服务器
const uploadFilePromise = (url) => {
	return new Promise((resolve, reject) => {
		let a = uni.uploadFile({
			url: store.uploadUrl,
			filePath: url,
			name: 'file',
			formData: {
				user: 'test'
			},
			header: {
				Authorization: store.token ? `Bearer ${store.token}` : ''
			},
			success: (res) => {
				if (res.statusCode === 200) {
					let data = JSON.parse(res.data)
					if (data.code === 200) {
						// console.log('上传图片成功', data.data);
						resolve(data.data.url)
					}
				}
			},
			complete() {
				uni.hideLoading()
			}
		});
	})
}

// 每次随机生成一个可以当做 id 的不会重复的值：a4168e6598df6
export function getId() {
	return Math.random().toString(16).slice(2);
}

// 格式化时间函数，接收一个长度为 10 位 或者 13 位 的时间戳
export function getDate(time = Number(new Date()), isGetText = true) {
	// 如果时间戳是 10位 的就 * 1000 转换为 毫秒
	const _time = time.toString().length > 10 ? time : time * 1000
	// 这里传入的时间戳是 10 位的，表示为秒数，所以要乘以 1000 转换为毫秒
	var date = new Date(_time);
	var Y = date.getFullYear();
	var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
	var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
	var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
	var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
	var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	// 处理与当前时间相比较的时间格式，比如：3分钟前、1小时前、1天前...
	let text = ''
	if (isGetText) {
		let nd = getDate(undefined, false) // 获取现在的时间信息
		if (Y == nd.Y) {
			if (M == nd.M) {
				if (D == nd.D) {
					if (h == nd.h) { // 同一个小时内
						let mc = nd.m - m
						if (mc > 0) {
							text = `${mc}分钟前`
						} else {
							text = `刚刚` // 一分钟内的时间返回 "刚刚"
						}
					} else {
						text = `${nd.h - h}小时前`
					}
				} else {
					// 判断是否为昨天
					if (D == nd.D - 1) {
						text = `昨天 ${h}:${m}`
					} else {
						text = `${Y}年${M}月${D}日 ${h}:${m}`
					}
				}
			} else {
				text = `${Y}年${M}月${D}日 ${h}:${m}`
			}
		} else {
			text = `${Y}年${M}月${D}日 ${h}:${m}`
		}
	}


	// 返回一个对象
	let obj = {
		date: `${Y}年${M}月${D}日`,
		time: `${h}时${m}分${s}秒`,
		Y,
		M,
		D,
		h,
		m,
		s,
		text // 这里返回的是类似于 1分钟前 这种文字
	}

	return obj;
}

// 封装一个验证手机号的方法
export function isPhone(phone) {
	// 校验手机号的正则
	let reg = /^1[3|4|5|6|7|8][0-9]{9}$/;
	if (reg.test(phone)) {
		return true
	}
	// uni.$u.toast('手机号格式错误')
	return false
}
// 拨号
export function call(phone) {
	if (isPhone(phone)) {
		uni.makePhoneCall({
			phoneNumber: phone
		})
	}
}
// 复制
export function copy(text) {
	if (text) {
		uni.setClipboardData({
			data: text,
			showToast: true // 弹出提示
		})
	}
}
/**
 * 图片预览
 * current 为当前显示图片的链接/索引值，不填或填写的值无效则为 urls 的第一张
 * urls 需要预览的图片链接列表。注：如果传入的是一个对象数据，则将图片链接在对象中的字段名作为第三个字段传入
 * key 如果urls是对象数组则会使用 key 在对象中取出链接字段的值
 */
export function previewImgs(current, urls, key = '') {
	if (key) {
		if (urls.length && typeof urls[0] == 'object') {
			let list = [...urls]
			urls = []
			list.forEach((item, index) => {
				if (item[key]) urls.push(item[key])
			})
		}
		if (typeof current == 'object') {
			current = current[key]
		}
	}
	uni.previewImage({
		urls,
		current
	})
}