export default {
	data() {
		return {
			// 默认的全局分享内容
			share: {
				title: '标题',
				path: '/pages/home/home', // 这里是分享的页面
				imageUrl: '' // 这里是分析的图片，如果不写则默认截取当前分享页为分享图片
			}
		}
	},
	// 定义全局分享
	// 1.发送给朋友
	onShareAppMessage(res) {
		return {
			title: this.share.title,
			path: this.share.path,
			imageUrl: this.share.imageUrl,
		}
	},
	//2.分享到朋友圈
	onShareTimeline(res) {
		return {
			title: this.share.title,
			path: this.share.path,
			imageUrl: this.share.imageUrl,
		}
	},
}
