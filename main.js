import App from './App'
// 这个框架引入了 uview-plus UI组件库，该组件库中的所有组件和方法均可使用
// uview-plus文档：https://uiadmin.net/uview-plus/
import uviewPlus from '@/uni_modules/uview-plus'
// 全局配置微信小程序分享
import share from '/config/share.js'
import {
	initRequest
} from '@/config/request.js'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
// 导入 pinia 全局状态管理
import {
	createPinia
} from 'pinia'
export function createApp() {
	const app = createSSRApp(App)
	const pinia = createPinia()
	app.use(pinia).use(uviewPlus)
	app.mixin(share)
    // 初始化封装的请求方法
	initRequest()
	return {
		app,
		pinia
	}
}
// #endif