import {useStore} from '@/store/uses.js'
import { createPinia } from 'pinia';
const pinia = createPinia();

/**
 * 导出 store 需要使用的页面直接
 * import { store } from '@/store'
 * 即可使用 store 中的属于和方法
 */
export const store = useStore(pinia)