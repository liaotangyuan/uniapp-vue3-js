"use strict";
require("../common/vendor.js");
require("./api.js");
require("../store/index.js");
function getDate(time = Number(/* @__PURE__ */ new Date()), isGetText = true) {
  const _time = time.toString().length > 10 ? time : time * 1e3;
  var date = new Date(_time);
  var Y = date.getFullYear();
  var M = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
  var D = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
  var h = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
  var m = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
  var s = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
  let text = "";
  if (isGetText) {
    let nd = getDate(void 0, false);
    if (Y == nd.Y) {
      if (M == nd.M) {
        if (D == nd.D) {
          if (h == nd.h) {
            let mc = nd.m - m;
            if (mc > 0) {
              text = `${mc}分钟前`;
            } else {
              text = `刚刚`;
            }
          } else {
            text = `${nd.h - h}小时前`;
          }
        } else {
          if (D == nd.D - 1) {
            text = `昨天 ${h}:${m}`;
          } else {
            text = `${Y}年${M}月${D}日 ${h}:${m}`;
          }
        }
      } else {
        text = `${Y}年${M}月${D}日 ${h}:${m}`;
      }
    } else {
      text = `${Y}年${M}月${D}日 ${h}:${m}`;
    }
  }
  let obj = {
    date: `${Y}年${M}月${D}日`,
    time: `${h}时${m}分${s}秒`,
    Y,
    M,
    D,
    h,
    m,
    s,
    text
    // 这里返回的是类似于 1分钟前 这种文字
  };
  return obj;
}
exports.getDate = getDate;
