"use strict";
const common_vendor = require("../common/vendor.js");
const store_index = require("../store/index.js");
function initRequest() {
  console.log("初始化了 http 请求代码");
  common_vendor.index.$u.http.setConfig((config) => {
    config.baseURL = `http://tf.hqzf100.com/dev-api/`;
    config.custom.toast = true;
    return config;
  });
  common_vendor.index.$u.http.interceptors.request.use((config) => {
    config.data = config.data || {};
    let token = store_index.store.token;
    config.header.Authorization = token ? `Bearer ${token}` : "";
    return config;
  }, (config) => {
    return Promise.reject(config);
  });
  common_vendor.index.$u.http.interceptors.response.use((response) => {
    var _a;
    const data = response.data;
    const custom = (_a = response.config) == null ? void 0 : _a.custom;
    if (data.code !== 200) {
      if (data.code === 401) {
        common_vendor.index.$u.toast(data.msg || "身份认证失败，请先登录");
        store_index.store.setToken("");
        setTimeout(() => {
          common_vendor.index.$u.route({
            type: "reLaunch",
            url: "/pages/login/login"
            // 跳转到登录页
          });
        }, 1e3);
      } else {
        if (custom.toast !== false) {
          common_vendor.index.$u.toast(data.msg);
        }
        return data;
      }
    }
    return data;
  }, (response) => {
    return Promise.reject(response);
  });
}
exports.initRequest = initRequest;
