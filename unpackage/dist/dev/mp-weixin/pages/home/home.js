"use strict";
const common_vendor = require("../../common/vendor.js");
const store_index = require("../../store/index.js");
const config_hooks = require("../../config/hooks.js");
require("../../store/uses.js");
require("../../config/api.js");
const _sfc_main = {
  __name: "home",
  setup(__props) {
    common_vendor.onLoad((args) => {
      console.log("首页收到的参数对象", args);
      if (!store_index.store.token) {
        console.log("没有读取到用户的token，用户还没有登录", store_index.store.token);
      }
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(common_vendor.unref(config_hooks.getDate)().date)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-07e72d3c"], ["__file", "F:/lty/uniapp-vue3-template/pages/home/home.vue"]]);
wx.createPage(MiniProgramPage);
