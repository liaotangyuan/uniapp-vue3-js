"use strict";
const store_uses = require("./uses.js");
const common_vendor = require("../common/vendor.js");
const pinia = common_vendor.createPinia();
const store = store_uses.useStore(pinia);
exports.store = store;
