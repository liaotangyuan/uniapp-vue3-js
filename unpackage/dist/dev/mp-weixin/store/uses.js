"use strict";
const common_vendor = require("../common/vendor.js");
const useStore = common_vendor.defineStore("user", {
  state: () => ({
    userInfo: {},
    // 用户信息
    token: "",
    // uploadUrl: 'http://192.168.1.3:8080/ai-file/upload/oss', // 上传资源的服务器网址
    uploadUrl: "https://gc.sdzcq.com/prod-api/ai-file/upload/oss"
    // 上传资源的服务器网址
  }),
  getters: {},
  actions: {
    setUserInfo(data) {
      if (data.userId) {
        console.log("更新全局的用户信息-也更新本地存储", data);
        this.userInfo = data;
        common_vendor.index.setStorageSync("userInfo", JSON.stringify(data));
      } else {
        common_vendor.index.removeStorageSync("userInfo");
      }
    },
    setToken(token) {
      if (token) {
        console.log("更新全局的 token -也更新本地存储", token);
        this.token = token;
        common_vendor.index.setStorageSync("token", token);
      } else {
        common_vendor.index.removeStorageSync("token");
      }
    }
  }
});
exports.useStore = useStore;
